<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Flash
 */

?>

		</div><!-- .tg-container -->
	</div><!-- #content -->

	<?php
	/**
	 * flash_after_main hook
	 */
	do_action( 'flash_after_main' ); ?>

	<?php
	/**
	 * flash_before_footer hook
	 */
	do_action( 'flash_before_footer' ); ?>

	<footer id="colophon" class="footer-layout site-footer" role="contentinfo">
		<?php get_sidebar( 'footer' ); ?>

		<div id="bottom-footer">
			<div class="tg-container">
			</div>

		<div id="bottom-footer3">
			<div class="tg-container">
			<div class="footer123 col-sm-4">
	          <h4 class="titlefooter">Contact Us</h4>
	            <p class="batas titlefooter newfont" ><a href="#hidden2" data-toggle="collapse" >Marketing</a></p>
	                <div id="hidden2" class="collapse" >
	                  <p class="font2 bagan">
	                    Jl. cantel no 15<br>
	                        Umbulharjo , mujamuju<br>
	                        Daerah Istimewa Yogakarta<br>
	                        55165<br>
	                        +620293887654
	                  </p>
	                </div>
	              <p class="batas titlefooter newfont"><a href="#hidden3" data-toggle="collapse">Customer Service</a></p>
	                <div id="hidden3" class="collapse" >
	                  <p class="font2 bagan">
	                    Jl. cantel no 15<br>
	                        Umbulharjo , mujamuju<br>
	                        Daerah Istimewa Yogakarta<br>
	                        55165<br>
	                        +620293887654
	                  </p>
	                </div>
	                <p class="batas titlefooter newfont"><a href="#hidden4" data-toggle="collapse">PIC</a></>
	                  <div id="hidden4" class="collapse" >
	                    <p class="font2 bagan">
	                      Jl. cantel no 15<br>
	                          Umbulharjo , mujamuju<br>
	                          Daerah Istimewa Yogakarta<br>
	                          55165<br>
	                          +620293887654
	                    </p>
	                  </div>
	                  <br class="batas">
	      	</div>
			<div class="footer1234 col-sm-4">
					<h4 class="titlefooter">Support</h4>
					<br>
					<img src="http://localhost/wordpress/wp-content/uploads/2018/09/logoASW.png" width="120" height="120" >
					<img src="http://localhost/wordpress/wp-content/uploads/2018/09/logoNumiteg.png" width="120" height="120" >
			</div>
			<div class="footer123 col-sm-4">
					<h4 class="titlefooter">Sosial Media</h4>
					<br>
					<div class="textfaAlign">
					<h4><a href="#" class="textfaAlign fa fa-facebook"></a>
					<a href="#" class="textfaAlign fa fa-envelope"></a>
					<a href="#" class="textfaAlign fa fa-instagram"></a></h4>
					</div>
					<br>
					<h4> Download Aplikasi</h4>
					<img src="http://localhost/wordpress/wp-content/uploads/2018/09/googleplay.png" height="80" width="170">
			</div>
		</div>

		<div id="bottom-footer2">
			<div class="tg-container">
				<div class="copyright">
					<div class="copyright-text">
						<a>Copyright &copy; 2018. ASW Reload - HRM. All right reserved.
						Develop by numiteg</a>
			</div>
		</div>
	</footer><!-- #colophon -->

	<?php
	/**
	 * flash_after_footer hook
	 */
	do_action( 'flash_after_footer' ); ?>

	<?php if ( get_theme_mod( 'flash_disable_back_to_top', '' ) != 1 ) : ?>
	<a href="#masthead" id="scroll-up"><i class="fa fa-chevron-up"></i></a>
	<?php endif; ?>
</div><!-- #page -->

<?php
/**
 * flash_after hook
 */
do_action( 'flash_after' ); ?>

<?php wp_footer(); ?>

</body>
</html>
