<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Flash
 */

get_header(); ?>


	<div class="carousel-inner wallheader" role="listbox">
		<!-- <div class="jumbotron"> -->
		  <img src="http://localhost/wordpress/wp-content/uploads/2018/09/wallpapersteppay.png" class="hilang">
			<div class="carousel-caption jumset">
		      <h3>Aplikasi yang memudahkan <br></h3>
		      <h3>anda dalam pembayaran <br></h3>
		      <h3>Online</h3>
		      <br>
					<a class="btn btn-lg btn-primary" href="#" role="button"><span class="fa fa-download" size="50"></span> Download App</a>
		    </div>
	</div>

	<div class="konten">
      <h2>Produk</h2>
      <br>
       <div class="row">
          <div class="col-sm-2">
              <img src="http://localhost/wordpress/wp-content/uploads/2018/10/telkomsel.png" >
          </div>
          <div class="col-sm-2">
              <img src="http://localhost/wordpress/wp-content/uploads/2018/10/indosat.png" >
          </div>
          <div class="col-sm-2">
              <img src="http://localhost/wordpress/wp-content/uploads/2018/10/xlaxis.png" >
          </div>
          <div class="col-sm-2">
              <img src="http://localhost/wordpress/wp-content/uploads/2018/10/three.png" >
          </div>
          <div class="col-sm-2">
              <img src="http://localhost/wordpress/wp-content/uploads/2018/10/smartfreen.png" >
          </div>
          <div class="col-sm-2">
              <img src="http://localhost/wordpress/wp-content/uploads/2018/10/grab.png" >
          </div>
       </div>
       <br>
       <div class="row">
             <div class="col-sm-2">
                 <img src="http://localhost/wordpress/wp-content/uploads/2018/10/gojek.png" >
             </div>
             <div class="col-sm-2">
                 <img src="http://localhost/wordpress/wp-content/uploads/2018/10/mandiri.png" >
             </div>
             <div class="col-sm-2">
                 <img src="http://localhost/wordpress/wp-content/uploads/2018/10/bni.png" >
             </div>
      </div>
      <br>
	</div>
	<?php
	/**
	 * flash_before_body_content hook
	 */
	do_action( 'flash_before_body_content' ); ?>

	<?php
	/**
	 * flash_after_body_content hook
	 */
	do_action( 'flash_after_body_content' ); ?>

<?php echo do_shortcode('[metaslider id="182"]'); ?>
<?php

get_footer();
